#include <SoftwareSerial.h>
#include <Wire.h>
#include <DFRobot_sim808.h>

#define PIN_TX 10
#define PIN_RX 11
#define PHONE_NUMBER "0031642467778"  


#define MESSAGE_LENGTH 160
char message[MESSAGE_LENGTH];
int messageIndex = 0;
char servermessage[300];
char Latitude[12];
char Longitude[12];
char MESSAGE[300];
char lat[12];
char lon[12];
char wspeed[12];
char token_string[20] = "WQNELKBJUI";

char phone[16];
char datetime[24];
SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);//Connect RX,TX,PWR,
String apn = "live.vodafone.com";          //APN
String apn_u = "ppp";                     //APN-Username
String apn_p = "ppp";                     //APN-Password
String url = "server.estonsecurity.nl/etrace/http/location";  //URL for HTTP-POST-REQUEST


void sendSMS();
void getGPS();
void readSMS();


void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  delay(10000);
}

void loop() { // run over and over

    getGPS(); 
    gsm_sendhttp(); //Start the GSM-Modul and start the transmisson
    sendSMS();
    delay(60000); //Wait one minute

}


void gsm_sendhttp() {
  
  
  mySerial.println("AT");
  runsl();//Print GSM Status an the Serial Output;
  delay(4000);
  mySerial.println("AT+SAPBR=3,1,Contype,GPRS");
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=3,1,APN," + apn);
  runsl();
  delay(100);
  //mySerial.println("AT+SAPBR=3,1,USER," + apn_u); 
  //runsl();
  //delay(100);
  //mySerial.println("AT+SAPBR=3,1,PWD," + apn_p);
  //runsl();
  delay(100);
  mySerial.println("AT+SAPBR =1,1");
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=2,1");
  runsl();
  delay(2000);
  mySerial.println("AT+HTTPINIT");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=CID,1");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=URL," + url);
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=CONTENT,application/x-www-form-urlencoded");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPDATA=192,15000");
  runsl();
  delay(100);
  mySerial.println(String(servermessage));
  runsl();
  delay(15000);
  mySerial.println("AT+HTTPACTION=1");
  runsl();
  delay(5000);
  mySerial.println("AT+HTTPREAD");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPTERM");
  runsl(); 
  
}

//Print GSM Status
void runsl() {
  while (mySerial.available()) {
    Serial.write(mySerial.read());
  }
}

void getGPS()
{ 
  while(!sim808.attachGPS())
  {
    Serial.println("Open the GPS power failure");
    delay(1000);
  }
  delay(3000);

  Serial.println("Open the GPS power success");
    
  while(!sim808.getGPS())
  {
    
  }

  Serial.print(sim808.GPSdata.year);
  Serial.print("/");
  Serial.print(sim808.GPSdata.month);
  Serial.print("/");
  Serial.print(sim808.GPSdata.day);
  Serial.print(" ");
  Serial.print(sim808.GPSdata.hour);
  Serial.print(":");
  Serial.print(sim808.GPSdata.minute);
  Serial.print(":");
  Serial.print(sim808.GPSdata.second);
  Serial.print(":");
  Serial.println(sim808.GPSdata.centisecond);
  Serial.print("latitude :");
  Serial.println(sim808.GPSdata.lat);
  Serial.print("longitude :");
  Serial.println(sim808.GPSdata.lon);
  Serial.print("speed_kph :");
  Serial.println(sim808.GPSdata.speed_kph);
  Serial.print("heading :");
  Serial.println(sim808.GPSdata.heading);
  Serial.println();

  float la = sim808.GPSdata.lat;
  float lo = sim808.GPSdata.lon;
  float ws = sim808.GPSdata.speed_kph;

  dtostrf(la, 4, 6, lat); //put float value of la into char array of lat. 4 = number of digits before decimal sign. 6 = number of digits after the decimal sign.
  dtostrf(lo, 4, 6, lon); //put float value of lo into char array of lon
  dtostrf(ws, 6, 2, wspeed);  //put float value of ws into char array of wspeed

  
  //sprintf(Latitude, lat,lat);
  //sprintf(Longitude, lon, lon);
  //sprintf(MESSAGE, "Latitude : %s\nLongitude :  %s\n", lat, lon);

  sprintf(servermessage, "{'type': 'location', 'data': {'wearable':  '%s', 'latitude':  %s, 'longitude':  %s}}",token_string, lat, lon);
  sprintf(MESSAGE, "Patient : OL : Is out of bounds. Please sign in to \nhttps://etrace.estonsecurity.nl  \nTo locate the patient");

}

void sendSMS()
{
  Serial.println("Start to send message ...");
  Serial.println(MESSAGE);
  Serial.println(PHONE_NUMBER);
  sim808.sendSMS(PHONE_NUMBER,MESSAGE);
}

